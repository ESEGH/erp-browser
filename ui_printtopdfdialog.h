/********************************************************************************
** Form generated from reading UI file 'printtopdfdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRINTTOPDFDIALOG_H
#define UI_PRINTTOPDFDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_PrintToPdfDialog
{
public:
    QGridLayout *gridLayout_2;
    QDialogButtonBox *buttonBox;
    QGridLayout *gridLayout;
    QToolButton *choosePageLayoutButton;
    QLabel *filePathLabel;
    QLineEdit *filePathLineEdit;
    QLabel *layoutLabel;
    QToolButton *chooseFilePathButton;
    QLabel *pageLayoutLabel;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *PrintToPdfDialog)
    {
        if (PrintToPdfDialog->objectName().isEmpty())
            PrintToPdfDialog->setObjectName(QStringLiteral("PrintToPdfDialog"));
        PrintToPdfDialog->resize(372, 117);
        gridLayout_2 = new QGridLayout(PrintToPdfDialog);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        buttonBox = new QDialogButtonBox(PrintToPdfDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_2->addWidget(buttonBox, 2, 0, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        choosePageLayoutButton = new QToolButton(PrintToPdfDialog);
        choosePageLayoutButton->setObjectName(QStringLiteral("choosePageLayoutButton"));

        gridLayout->addWidget(choosePageLayoutButton, 1, 3, 1, 1);

        filePathLabel = new QLabel(PrintToPdfDialog);
        filePathLabel->setObjectName(QStringLiteral("filePathLabel"));

        gridLayout->addWidget(filePathLabel, 0, 0, 1, 1);

        filePathLineEdit = new QLineEdit(PrintToPdfDialog);
        filePathLineEdit->setObjectName(QStringLiteral("filePathLineEdit"));

        gridLayout->addWidget(filePathLineEdit, 0, 2, 1, 1);

        layoutLabel = new QLabel(PrintToPdfDialog);
        layoutLabel->setObjectName(QStringLiteral("layoutLabel"));

        gridLayout->addWidget(layoutLabel, 1, 0, 1, 1);

        chooseFilePathButton = new QToolButton(PrintToPdfDialog);
        chooseFilePathButton->setObjectName(QStringLiteral("chooseFilePathButton"));

        gridLayout->addWidget(chooseFilePathButton, 0, 3, 1, 1);

        pageLayoutLabel = new QLabel(PrintToPdfDialog);
        pageLayoutLabel->setObjectName(QStringLiteral("pageLayoutLabel"));

        gridLayout->addWidget(pageLayoutLabel, 1, 2, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 1, 0, 1, 1);


        retranslateUi(PrintToPdfDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PrintToPdfDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PrintToPdfDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(PrintToPdfDialog);
    } // setupUi

    void retranslateUi(QDialog *PrintToPdfDialog)
    {
        PrintToPdfDialog->setWindowTitle(QApplication::translate("PrintToPdfDialog", "Dialog", 0));
        choosePageLayoutButton->setText(QApplication::translate("PrintToPdfDialog", "...", 0));
        filePathLabel->setText(QApplication::translate("PrintToPdfDialog", "Save as:", 0));
        layoutLabel->setText(QApplication::translate("PrintToPdfDialog", "Page layout:", 0));
        chooseFilePathButton->setText(QApplication::translate("PrintToPdfDialog", "...", 0));
        pageLayoutLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class PrintToPdfDialog: public Ui_PrintToPdfDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRINTTOPDFDIALOG_H
