/****************************************************************************
** Meta object code from reading C++ file 'browserapplication.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../browserapplication.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'browserapplication.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_BrowserApplication_t {
    QByteArrayData data[13];
    char stringdata0[191];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BrowserApplication_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BrowserApplication_t qt_meta_stringdata_BrowserApplication = {
    {
QT_MOC_LITERAL(0, 0, 18), // "BrowserApplication"
QT_MOC_LITERAL(1, 19, 22), // "privateBrowsingChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 13), // "newMainWindow"
QT_MOC_LITERAL(4, 57, 18), // "BrowserMainWindow*"
QT_MOC_LITERAL(5, 76, 18), // "restoreLastSession"
QT_MOC_LITERAL(6, 95, 16), // "lastWindowClosed"
QT_MOC_LITERAL(7, 112, 11), // "quitBrowser"
QT_MOC_LITERAL(8, 124, 18), // "setPrivateBrowsing"
QT_MOC_LITERAL(9, 143, 10), // "postLaunch"
QT_MOC_LITERAL(10, 154, 7), // "openUrl"
QT_MOC_LITERAL(11, 162, 3), // "url"
QT_MOC_LITERAL(12, 166, 24) // "newLocalSocketConnection"

    },
    "BrowserApplication\0privateBrowsingChanged\0"
    "\0newMainWindow\0BrowserMainWindow*\0"
    "restoreLastSession\0lastWindowClosed\0"
    "quitBrowser\0setPrivateBrowsing\0"
    "postLaunch\0openUrl\0url\0newLocalSocketConnection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BrowserApplication[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   62,    2, 0x0a /* Public */,
       5,    0,   63,    2, 0x0a /* Public */,
       6,    0,   64,    2, 0x0a /* Public */,
       7,    0,   65,    2, 0x0a /* Public */,
       8,    1,   66,    2, 0x0a /* Public */,
       9,    0,   69,    2, 0x08 /* Private */,
      10,    1,   70,    2, 0x08 /* Private */,
      12,    0,   73,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    2,

 // slots: parameters
    0x80000000 | 4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QUrl,   11,
    QMetaType::Void,

       0        // eod
};

void BrowserApplication::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BrowserApplication *_t = static_cast<BrowserApplication *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->privateBrowsingChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: { BrowserMainWindow* _r = _t->newMainWindow();
            if (_a[0]) *reinterpret_cast< BrowserMainWindow**>(_a[0]) = _r; }  break;
        case 2: _t->restoreLastSession(); break;
        case 3: _t->lastWindowClosed(); break;
        case 4: _t->quitBrowser(); break;
        case 5: _t->setPrivateBrowsing((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->postLaunch(); break;
        case 7: _t->openUrl((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 8: _t->newLocalSocketConnection(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (BrowserApplication::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BrowserApplication::privateBrowsingChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject BrowserApplication::staticMetaObject = {
    { &QApplication::staticMetaObject, qt_meta_stringdata_BrowserApplication.data,
      qt_meta_data_BrowserApplication,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *BrowserApplication::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BrowserApplication::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_BrowserApplication.stringdata0))
        return static_cast<void*>(const_cast< BrowserApplication*>(this));
    return QApplication::qt_metacast(_clname);
}

int BrowserApplication::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QApplication::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void BrowserApplication::privateBrowsingChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
