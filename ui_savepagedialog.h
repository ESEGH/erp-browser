/********************************************************************************
** Form generated from reading UI file 'savepagedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SAVEPAGEDIALOG_H
#define UI_SAVEPAGEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SavePageDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label;
    QComboBox *formatComboBox;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QLineEdit *filePathLineEdit;
    QToolButton *chooseFilePathButton;
    QSpacerItem *verticalSpacer;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SavePageDialog)
    {
        if (SavePageDialog->objectName().isEmpty())
            SavePageDialog->setObjectName(QStringLiteral("SavePageDialog"));
        SavePageDialog->resize(400, 121);
        verticalLayout = new QVBoxLayout(SavePageDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(SavePageDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        formatComboBox = new QComboBox(SavePageDialog);
        formatComboBox->setObjectName(QStringLiteral("formatComboBox"));

        gridLayout->addWidget(formatComboBox, 0, 1, 1, 1);

        label_2 = new QLabel(SavePageDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        filePathLineEdit = new QLineEdit(SavePageDialog);
        filePathLineEdit->setObjectName(QStringLiteral("filePathLineEdit"));

        horizontalLayout->addWidget(filePathLineEdit);

        chooseFilePathButton = new QToolButton(SavePageDialog);
        chooseFilePathButton->setObjectName(QStringLiteral("chooseFilePathButton"));

        horizontalLayout->addWidget(chooseFilePathButton);


        gridLayout->addLayout(horizontalLayout, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 12, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        buttonBox = new QDialogButtonBox(SavePageDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);

#ifndef QT_NO_SHORTCUT
        label->setBuddy(formatComboBox);
        label_2->setBuddy(filePathLineEdit);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(formatComboBox, filePathLineEdit);
        QWidget::setTabOrder(filePathLineEdit, chooseFilePathButton);

        retranslateUi(SavePageDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SavePageDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SavePageDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SavePageDialog);
    } // setupUi

    void retranslateUi(QDialog *SavePageDialog)
    {
        SavePageDialog->setWindowTitle(QApplication::translate("SavePageDialog", "Dialog", 0));
        label->setText(QApplication::translate("SavePageDialog", "&Format:", 0));
        formatComboBox->clear();
        formatComboBox->insertItems(0, QStringList()
         << QApplication::translate("SavePageDialog", "Single HTML", 0)
         << QApplication::translate("SavePageDialog", "Complete HTML", 0)
         << QApplication::translate("SavePageDialog", "MIME HTML", 0)
        );
        label_2->setText(QApplication::translate("SavePageDialog", "&Save to:", 0));
        chooseFilePathButton->setText(QApplication::translate("SavePageDialog", "...", 0));
    } // retranslateUi

};

namespace Ui {
    class SavePageDialog: public Ui_SavePageDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SAVEPAGEDIALOG_H
