/****************************************************************************
** Meta object code from reading C++ file 'featurepermissionbar.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../featurepermissionbar.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'featurepermissionbar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_FeaturePermissionBar_t {
    QByteArrayData data[9];
    char stringdata0[173];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FeaturePermissionBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FeaturePermissionBar_t qt_meta_stringdata_FeaturePermissionBar = {
    {
QT_MOC_LITERAL(0, 0, 20), // "FeaturePermissionBar"
QT_MOC_LITERAL(1, 21, 25), // "featurePermissionProvided"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 14), // "securityOrigin"
QT_MOC_LITERAL(4, 63, 23), // "QWebEnginePage::Feature"
QT_MOC_LITERAL(5, 87, 32), // "QWebEnginePage::PermissionPolicy"
QT_MOC_LITERAL(6, 120, 16), // "permissionDenied"
QT_MOC_LITERAL(7, 137, 17), // "permissionGranted"
QT_MOC_LITERAL(8, 155, 17) // "permissionUnknown"

    },
    "FeaturePermissionBar\0featurePermissionProvided\0"
    "\0securityOrigin\0QWebEnginePage::Feature\0"
    "QWebEnginePage::PermissionPolicy\0"
    "permissionDenied\0permissionGranted\0"
    "permissionUnknown"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FeaturePermissionBar[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   34,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   41,    2, 0x08 /* Private */,
       7,    0,   42,    2, 0x08 /* Private */,
       8,    0,   43,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QUrl, 0x80000000 | 4, 0x80000000 | 5,    3,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void FeaturePermissionBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FeaturePermissionBar *_t = static_cast<FeaturePermissionBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->featurePermissionProvided((*reinterpret_cast< const QUrl(*)>(_a[1])),(*reinterpret_cast< QWebEnginePage::Feature(*)>(_a[2])),(*reinterpret_cast< QWebEnginePage::PermissionPolicy(*)>(_a[3]))); break;
        case 1: _t->permissionDenied(); break;
        case 2: _t->permissionGranted(); break;
        case 3: _t->permissionUnknown(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (FeaturePermissionBar::*_t)(const QUrl & , QWebEnginePage::Feature , QWebEnginePage::PermissionPolicy );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FeaturePermissionBar::featurePermissionProvided)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject FeaturePermissionBar::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_FeaturePermissionBar.data,
      qt_meta_data_FeaturePermissionBar,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FeaturePermissionBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FeaturePermissionBar::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FeaturePermissionBar.stringdata0))
        return static_cast<void*>(const_cast< FeaturePermissionBar*>(this));
    return QWidget::qt_metacast(_clname);
}

int FeaturePermissionBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void FeaturePermissionBar::featurePermissionProvided(const QUrl & _t1, QWebEnginePage::Feature _t2, QWebEnginePage::PermissionPolicy _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
