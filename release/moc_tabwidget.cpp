/****************************************************************************
** Meta object code from reading C++ file 'tabwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tabwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tabwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_TabBar_t {
    QByteArrayData data[18];
    char stringdata0[182];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TabBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TabBar_t qt_meta_stringdata_TabBar = {
    {
QT_MOC_LITERAL(0, 0, 6), // "TabBar"
QT_MOC_LITERAL(1, 7, 6), // "newTab"
QT_MOC_LITERAL(2, 14, 0), // ""
QT_MOC_LITERAL(3, 15, 8), // "cloneTab"
QT_MOC_LITERAL(4, 24, 5), // "index"
QT_MOC_LITERAL(5, 30, 8), // "closeTab"
QT_MOC_LITERAL(6, 39, 14), // "closeOtherTabs"
QT_MOC_LITERAL(7, 54, 9), // "reloadTab"
QT_MOC_LITERAL(8, 64, 7), // "muteTab"
QT_MOC_LITERAL(9, 72, 4), // "mute"
QT_MOC_LITERAL(10, 77, 13), // "reloadAllTabs"
QT_MOC_LITERAL(11, 91, 16), // "tabMoveRequested"
QT_MOC_LITERAL(12, 108, 9), // "fromIndex"
QT_MOC_LITERAL(13, 118, 7), // "toIndex"
QT_MOC_LITERAL(14, 126, 15), // "selectTabAction"
QT_MOC_LITERAL(15, 142, 9), // "unmuteTab"
QT_MOC_LITERAL(16, 152, 20), // "contextMenuRequested"
QT_MOC_LITERAL(17, 173, 8) // "position"

    },
    "TabBar\0newTab\0\0cloneTab\0index\0closeTab\0"
    "closeOtherTabs\0reloadTab\0muteTab\0mute\0"
    "reloadAllTabs\0tabMoveRequested\0fromIndex\0"
    "toIndex\0selectTabAction\0unmuteTab\0"
    "contextMenuRequested\0position"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TabBar[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x06 /* Public */,
       3,    1,   95,    2, 0x06 /* Public */,
       5,    1,   98,    2, 0x06 /* Public */,
       6,    1,  101,    2, 0x06 /* Public */,
       7,    1,  104,    2, 0x06 /* Public */,
       8,    2,  107,    2, 0x06 /* Public */,
      10,    0,  112,    2, 0x06 /* Public */,
      11,    2,  113,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    0,  118,    2, 0x08 /* Private */,
       3,    0,  119,    2, 0x08 /* Private */,
       5,    0,  120,    2, 0x08 /* Private */,
       6,    0,  121,    2, 0x08 /* Private */,
       7,    0,  122,    2, 0x08 /* Private */,
       8,    0,  123,    2, 0x08 /* Private */,
      15,    0,  124,    2, 0x08 /* Private */,
      16,    1,  125,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,    4,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   12,   13,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   17,

       0        // eod
};

void TabBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TabBar *_t = static_cast<TabBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newTab(); break;
        case 1: _t->cloneTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->closeTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->closeOtherTabs((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->reloadTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->muteTab((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 6: _t->reloadAllTabs(); break;
        case 7: _t->tabMoveRequested((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: _t->selectTabAction(); break;
        case 9: _t->cloneTab(); break;
        case 10: _t->closeTab(); break;
        case 11: _t->closeOtherTabs(); break;
        case 12: _t->reloadTab(); break;
        case 13: _t->muteTab(); break;
        case 14: _t->unmuteTab(); break;
        case 15: _t->contextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (TabBar::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabBar::newTab)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (TabBar::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabBar::cloneTab)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (TabBar::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabBar::closeTab)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (TabBar::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabBar::closeOtherTabs)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (TabBar::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabBar::reloadTab)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (TabBar::*_t)(int , bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabBar::muteTab)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (TabBar::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabBar::reloadAllTabs)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (TabBar::*_t)(int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabBar::tabMoveRequested)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject TabBar::staticMetaObject = {
    { &QTabBar::staticMetaObject, qt_meta_stringdata_TabBar.data,
      qt_meta_data_TabBar,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TabBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TabBar::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TabBar.stringdata0))
        return static_cast<void*>(const_cast< TabBar*>(this));
    return QTabBar::qt_metacast(_clname);
}

int TabBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTabBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void TabBar::newTab()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void TabBar::cloneTab(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TabBar::closeTab(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void TabBar::closeOtherTabs(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void TabBar::reloadTab(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void TabBar::muteTab(int _t1, bool _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void TabBar::reloadAllTabs()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void TabBar::tabMoveRequested(int _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
struct qt_meta_stringdata_WebActionMapper_t {
    QByteArrayData data[6];
    char stringdata0[75];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WebActionMapper_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WebActionMapper_t qt_meta_stringdata_WebActionMapper = {
    {
QT_MOC_LITERAL(0, 0, 15), // "WebActionMapper"
QT_MOC_LITERAL(1, 16, 13), // "rootTriggered"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 12), // "childChanged"
QT_MOC_LITERAL(4, 44, 13), // "rootDestroyed"
QT_MOC_LITERAL(5, 58, 16) // "currentDestroyed"

    },
    "WebActionMapper\0rootTriggered\0\0"
    "childChanged\0rootDestroyed\0currentDestroyed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WebActionMapper[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x08 /* Private */,
       3,    0,   35,    2, 0x08 /* Private */,
       4,    0,   36,    2, 0x08 /* Private */,
       5,    0,   37,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void WebActionMapper::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        WebActionMapper *_t = static_cast<WebActionMapper *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->rootTriggered(); break;
        case 1: _t->childChanged(); break;
        case 2: _t->rootDestroyed(); break;
        case 3: _t->currentDestroyed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject WebActionMapper::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_WebActionMapper.data,
      qt_meta_data_WebActionMapper,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *WebActionMapper::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WebActionMapper::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_WebActionMapper.stringdata0))
        return static_cast<void*>(const_cast< WebActionMapper*>(this));
    return QObject::qt_metacast(_clname);
}

int WebActionMapper::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_TabWidget_t {
    QByteArrayData data[58];
    char stringdata0[856];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TabWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TabWidget_t qt_meta_stringdata_TabWidget = {
    {
QT_MOC_LITERAL(0, 0, 9), // "TabWidget"
QT_MOC_LITERAL(1, 10, 8), // "loadPage"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 3), // "url"
QT_MOC_LITERAL(4, 24, 11), // "tabsChanged"
QT_MOC_LITERAL(5, 36, 13), // "lastTabClosed"
QT_MOC_LITERAL(6, 50, 15), // "setCurrentTitle"
QT_MOC_LITERAL(7, 66, 20), // "showStatusBarMessage"
QT_MOC_LITERAL(8, 87, 7), // "message"
QT_MOC_LITERAL(9, 95, 11), // "linkHovered"
QT_MOC_LITERAL(10, 107, 4), // "link"
QT_MOC_LITERAL(11, 112, 12), // "loadProgress"
QT_MOC_LITERAL(12, 125, 8), // "progress"
QT_MOC_LITERAL(13, 134, 23), // "geometryChangeRequested"
QT_MOC_LITERAL(14, 158, 8), // "geometry"
QT_MOC_LITERAL(15, 167, 32), // "menuBarVisibilityChangeRequested"
QT_MOC_LITERAL(16, 200, 7), // "visible"
QT_MOC_LITERAL(17, 208, 34), // "statusBarVisibilityChangeRequ..."
QT_MOC_LITERAL(18, 243, 32), // "toolBarVisibilityChangeRequested"
QT_MOC_LITERAL(19, 276, 19), // "loadUrlInCurrentTab"
QT_MOC_LITERAL(20, 296, 6), // "newTab"
QT_MOC_LITERAL(21, 303, 8), // "WebView*"
QT_MOC_LITERAL(22, 312, 11), // "makeCurrent"
QT_MOC_LITERAL(23, 324, 8), // "cloneTab"
QT_MOC_LITERAL(24, 333, 5), // "index"
QT_MOC_LITERAL(25, 339, 15), // "requestCloseTab"
QT_MOC_LITERAL(26, 355, 8), // "closeTab"
QT_MOC_LITERAL(27, 364, 14), // "closeOtherTabs"
QT_MOC_LITERAL(28, 379, 9), // "reloadTab"
QT_MOC_LITERAL(29, 389, 13), // "reloadAllTabs"
QT_MOC_LITERAL(30, 403, 7), // "nextTab"
QT_MOC_LITERAL(31, 411, 11), // "previousTab"
QT_MOC_LITERAL(32, 423, 19), // "setAudioMutedForTab"
QT_MOC_LITERAL(33, 443, 4), // "mute"
QT_MOC_LITERAL(34, 448, 14), // "currentChanged"
QT_MOC_LITERAL(35, 463, 25), // "aboutToShowRecentTabsMenu"
QT_MOC_LITERAL(36, 489, 32), // "aboutToShowRecentTriggeredAction"
QT_MOC_LITERAL(37, 522, 8), // "QAction*"
QT_MOC_LITERAL(38, 531, 6), // "action"
QT_MOC_LITERAL(39, 538, 17), // "downloadRequested"
QT_MOC_LITERAL(40, 556, 23), // "QWebEngineDownloadItem*"
QT_MOC_LITERAL(41, 580, 8), // "download"
QT_MOC_LITERAL(42, 589, 18), // "webViewLoadStarted"
QT_MOC_LITERAL(43, 608, 18), // "webViewIconChanged"
QT_MOC_LITERAL(44, 627, 4), // "icon"
QT_MOC_LITERAL(45, 632, 19), // "webViewTitleChanged"
QT_MOC_LITERAL(46, 652, 5), // "title"
QT_MOC_LITERAL(47, 658, 17), // "webViewUrlChanged"
QT_MOC_LITERAL(48, 676, 21), // "lineEditReturnPressed"
QT_MOC_LITERAL(49, 698, 20), // "windowCloseRequested"
QT_MOC_LITERAL(50, 719, 7), // "moveTab"
QT_MOC_LITERAL(51, 727, 9), // "fromIndex"
QT_MOC_LITERAL(52, 737, 7), // "toIndex"
QT_MOC_LITERAL(53, 745, 19), // "fullScreenRequested"
QT_MOC_LITERAL(54, 765, 27), // "QWebEngineFullScreenRequest"
QT_MOC_LITERAL(55, 793, 7), // "request"
QT_MOC_LITERAL(56, 801, 25), // "handleTabBarDoubleClicked"
QT_MOC_LITERAL(57, 827, 28) // "webPageMutedOrAudibleChanged"

    },
    "TabWidget\0loadPage\0\0url\0tabsChanged\0"
    "lastTabClosed\0setCurrentTitle\0"
    "showStatusBarMessage\0message\0linkHovered\0"
    "link\0loadProgress\0progress\0"
    "geometryChangeRequested\0geometry\0"
    "menuBarVisibilityChangeRequested\0"
    "visible\0statusBarVisibilityChangeRequested\0"
    "toolBarVisibilityChangeRequested\0"
    "loadUrlInCurrentTab\0newTab\0WebView*\0"
    "makeCurrent\0cloneTab\0index\0requestCloseTab\0"
    "closeTab\0closeOtherTabs\0reloadTab\0"
    "reloadAllTabs\0nextTab\0previousTab\0"
    "setAudioMutedForTab\0mute\0currentChanged\0"
    "aboutToShowRecentTabsMenu\0"
    "aboutToShowRecentTriggeredAction\0"
    "QAction*\0action\0downloadRequested\0"
    "QWebEngineDownloadItem*\0download\0"
    "webViewLoadStarted\0webViewIconChanged\0"
    "icon\0webViewTitleChanged\0title\0"
    "webViewUrlChanged\0lineEditReturnPressed\0"
    "windowCloseRequested\0moveTab\0fromIndex\0"
    "toIndex\0fullScreenRequested\0"
    "QWebEngineFullScreenRequest\0request\0"
    "handleTabBarDoubleClicked\0"
    "webPageMutedOrAudibleChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TabWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      40,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  214,    2, 0x06 /* Public */,
       4,    0,  217,    2, 0x06 /* Public */,
       5,    0,  218,    2, 0x06 /* Public */,
       6,    1,  219,    2, 0x06 /* Public */,
       7,    1,  222,    2, 0x06 /* Public */,
       9,    1,  225,    2, 0x06 /* Public */,
      11,    1,  228,    2, 0x06 /* Public */,
      13,    1,  231,    2, 0x06 /* Public */,
      15,    1,  234,    2, 0x06 /* Public */,
      17,    1,  237,    2, 0x06 /* Public */,
      18,    1,  240,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    1,  243,    2, 0x0a /* Public */,
      20,    1,  246,    2, 0x0a /* Public */,
      20,    0,  249,    2, 0x2a /* Public | MethodCloned */,
      23,    1,  250,    2, 0x0a /* Public */,
      23,    0,  253,    2, 0x2a /* Public | MethodCloned */,
      25,    1,  254,    2, 0x0a /* Public */,
      25,    0,  257,    2, 0x2a /* Public | MethodCloned */,
      26,    1,  258,    2, 0x0a /* Public */,
      27,    1,  261,    2, 0x0a /* Public */,
      28,    1,  264,    2, 0x0a /* Public */,
      28,    0,  267,    2, 0x2a /* Public | MethodCloned */,
      29,    0,  268,    2, 0x0a /* Public */,
      30,    0,  269,    2, 0x0a /* Public */,
      31,    0,  270,    2, 0x0a /* Public */,
      32,    2,  271,    2, 0x0a /* Public */,
      34,    1,  276,    2, 0x08 /* Private */,
      35,    0,  279,    2, 0x08 /* Private */,
      36,    1,  280,    2, 0x08 /* Private */,
      39,    1,  283,    2, 0x08 /* Private */,
      42,    0,  286,    2, 0x08 /* Private */,
      43,    1,  287,    2, 0x08 /* Private */,
      45,    1,  290,    2, 0x08 /* Private */,
      47,    1,  293,    2, 0x08 /* Private */,
      48,    0,  296,    2, 0x08 /* Private */,
      49,    0,  297,    2, 0x08 /* Private */,
      50,    2,  298,    2, 0x08 /* Private */,
      53,    1,  303,    2, 0x08 /* Private */,
      56,    1,  306,    2, 0x08 /* Private */,
      57,    0,  309,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::QRect,   14,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void, QMetaType::Bool,   16,

 // slots: parameters
    QMetaType::Void, QMetaType::QUrl,    3,
    0x80000000 | 21, QMetaType::Bool,   22,
    0x80000000 | 21,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,   24,   33,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 37,   38,
    QMetaType::Void, 0x80000000 | 40,   41,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QIcon,   44,
    QMetaType::Void, QMetaType::QString,   46,
    QMetaType::Void, QMetaType::QUrl,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   51,   52,
    QMetaType::Void, 0x80000000 | 54,   55,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void,

       0        // eod
};

void TabWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TabWidget *_t = static_cast<TabWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->loadPage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->tabsChanged(); break;
        case 2: _t->lastTabClosed(); break;
        case 3: _t->setCurrentTitle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->showStatusBarMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->linkHovered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->loadProgress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->geometryChangeRequested((*reinterpret_cast< const QRect(*)>(_a[1]))); break;
        case 8: _t->menuBarVisibilityChangeRequested((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->statusBarVisibilityChangeRequested((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->toolBarVisibilityChangeRequested((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->loadUrlInCurrentTab((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 12: { WebView* _r = _t->newTab((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< WebView**>(_a[0]) = _r; }  break;
        case 13: { WebView* _r = _t->newTab();
            if (_a[0]) *reinterpret_cast< WebView**>(_a[0]) = _r; }  break;
        case 14: _t->cloneTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->cloneTab(); break;
        case 16: _t->requestCloseTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->requestCloseTab(); break;
        case 18: _t->closeTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->closeOtherTabs((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->reloadTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->reloadTab(); break;
        case 22: _t->reloadAllTabs(); break;
        case 23: _t->nextTab(); break;
        case 24: _t->previousTab(); break;
        case 25: _t->setAudioMutedForTab((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 26: _t->currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->aboutToShowRecentTabsMenu(); break;
        case 28: _t->aboutToShowRecentTriggeredAction((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 29: _t->downloadRequested((*reinterpret_cast< QWebEngineDownloadItem*(*)>(_a[1]))); break;
        case 30: _t->webViewLoadStarted(); break;
        case 31: _t->webViewIconChanged((*reinterpret_cast< const QIcon(*)>(_a[1]))); break;
        case 32: _t->webViewTitleChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 33: _t->webViewUrlChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 34: _t->lineEditReturnPressed(); break;
        case 35: _t->windowCloseRequested(); break;
        case 36: _t->moveTab((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 37: _t->fullScreenRequested((*reinterpret_cast< QWebEngineFullScreenRequest(*)>(_a[1]))); break;
        case 38: _t->handleTabBarDoubleClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 39: _t->webPageMutedOrAudibleChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (TabWidget::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::loadPage)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::tabsChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::lastTabClosed)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::setCurrentTitle)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::showStatusBarMessage)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::linkHovered)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::loadProgress)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)(const QRect & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::geometryChangeRequested)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::menuBarVisibilityChangeRequested)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::statusBarVisibilityChangeRequested)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (TabWidget::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TabWidget::toolBarVisibilityChangeRequested)) {
                *result = 10;
                return;
            }
        }
    }
}

const QMetaObject TabWidget::staticMetaObject = {
    { &QTabWidget::staticMetaObject, qt_meta_stringdata_TabWidget.data,
      qt_meta_data_TabWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TabWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TabWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TabWidget.stringdata0))
        return static_cast<void*>(const_cast< TabWidget*>(this));
    return QTabWidget::qt_metacast(_clname);
}

int TabWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTabWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 40)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 40;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 40)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 40;
    }
    return _id;
}

// SIGNAL 0
void TabWidget::loadPage(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TabWidget::tabsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void TabWidget::lastTabClosed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void TabWidget::setCurrentTitle(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void TabWidget::showStatusBarMessage(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void TabWidget::linkHovered(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void TabWidget::loadProgress(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void TabWidget::geometryChangeRequested(const QRect & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void TabWidget::menuBarVisibilityChangeRequested(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void TabWidget::statusBarVisibilityChangeRequested(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void TabWidget::toolBarVisibilityChangeRequested(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}
QT_END_MOC_NAMESPACE
