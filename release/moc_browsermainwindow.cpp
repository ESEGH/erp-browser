/****************************************************************************
** Meta object code from reading C++ file 'browsermainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../browsermainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'browsermainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_BrowserMainWindow_t {
    QByteArrayData data[54];
    char stringdata0[870];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BrowserMainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BrowserMainWindow_t qt_meta_stringdata_BrowserMainWindow = {
    {
QT_MOC_LITERAL(0, 0, 17), // "BrowserMainWindow"
QT_MOC_LITERAL(1, 18, 8), // "loadPage"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 3), // "url"
QT_MOC_LITERAL(4, 32, 8), // "slotHome"
QT_MOC_LITERAL(5, 41, 4), // "save"
QT_MOC_LITERAL(6, 46, 16), // "slotLoadProgress"
QT_MOC_LITERAL(7, 63, 19), // "slotUpdateStatusbar"
QT_MOC_LITERAL(8, 83, 6), // "string"
QT_MOC_LITERAL(9, 90, 21), // "slotUpdateWindowTitle"
QT_MOC_LITERAL(10, 112, 5), // "title"
QT_MOC_LITERAL(11, 118, 7), // "loadUrl"
QT_MOC_LITERAL(12, 126, 15), // "slotPreferences"
QT_MOC_LITERAL(13, 142, 11), // "slotFileNew"
QT_MOC_LITERAL(14, 154, 12), // "slotFileOpen"
QT_MOC_LITERAL(15, 167, 20), // "slotFilePrintPreview"
QT_MOC_LITERAL(16, 188, 13), // "slotFilePrint"
QT_MOC_LITERAL(17, 202, 18), // "slotFilePrintToPDF"
QT_MOC_LITERAL(18, 221, 19), // "slotPrivateBrowsing"
QT_MOC_LITERAL(19, 241, 14), // "slotFileSaveAs"
QT_MOC_LITERAL(20, 256, 12), // "slotEditFind"
QT_MOC_LITERAL(21, 269, 16), // "slotEditFindNext"
QT_MOC_LITERAL(22, 286, 20), // "slotEditFindPrevious"
QT_MOC_LITERAL(23, 307, 23), // "slotShowBookmarksDialog"
QT_MOC_LITERAL(24, 331, 15), // "slotAddBookmark"
QT_MOC_LITERAL(25, 347, 14), // "slotViewZoomIn"
QT_MOC_LITERAL(26, 362, 15), // "slotViewZoomOut"
QT_MOC_LITERAL(27, 378, 17), // "slotViewResetZoom"
QT_MOC_LITERAL(28, 396, 15), // "slotViewToolbar"
QT_MOC_LITERAL(29, 412, 20), // "slotViewBookmarksBar"
QT_MOC_LITERAL(30, 433, 17), // "slotViewStatusbar"
QT_MOC_LITERAL(31, 451, 18), // "slotViewPageSource"
QT_MOC_LITERAL(32, 470, 18), // "slotViewFullScreen"
QT_MOC_LITERAL(33, 489, 6), // "enable"
QT_MOC_LITERAL(34, 496, 13), // "slotWebSearch"
QT_MOC_LITERAL(35, 510, 19), // "slotToggleInspector"
QT_MOC_LITERAL(36, 530, 20), // "slotAboutApplication"
QT_MOC_LITERAL(37, 551, 19), // "slotDownloadManager"
QT_MOC_LITERAL(38, 571, 18), // "slotSelectLineEdit"
QT_MOC_LITERAL(39, 590, 23), // "slotAboutToShowBackMenu"
QT_MOC_LITERAL(40, 614, 26), // "slotAboutToShowForwardMenu"
QT_MOC_LITERAL(41, 641, 25), // "slotAboutToShowWindowMenu"
QT_MOC_LITERAL(42, 667, 17), // "slotOpenActionUrl"
QT_MOC_LITERAL(43, 685, 8), // "QAction*"
QT_MOC_LITERAL(44, 694, 6), // "action"
QT_MOC_LITERAL(45, 701, 14), // "slotShowWindow"
QT_MOC_LITERAL(46, 716, 13), // "slotSwapFocus"
QT_MOC_LITERAL(47, 730, 20), // "slotHandlePdfPrinted"
QT_MOC_LITERAL(48, 751, 23), // "geometryChangeRequested"
QT_MOC_LITERAL(49, 775, 8), // "geometry"
QT_MOC_LITERAL(50, 784, 23), // "updateToolbarActionText"
QT_MOC_LITERAL(51, 808, 7), // "visible"
QT_MOC_LITERAL(52, 816, 32), // "updateBookmarksToolbarActionText"
QT_MOC_LITERAL(53, 849, 20) // "runScriptOnOpenViews"

    },
    "BrowserMainWindow\0loadPage\0\0url\0"
    "slotHome\0save\0slotLoadProgress\0"
    "slotUpdateStatusbar\0string\0"
    "slotUpdateWindowTitle\0title\0loadUrl\0"
    "slotPreferences\0slotFileNew\0slotFileOpen\0"
    "slotFilePrintPreview\0slotFilePrint\0"
    "slotFilePrintToPDF\0slotPrivateBrowsing\0"
    "slotFileSaveAs\0slotEditFind\0"
    "slotEditFindNext\0slotEditFindPrevious\0"
    "slotShowBookmarksDialog\0slotAddBookmark\0"
    "slotViewZoomIn\0slotViewZoomOut\0"
    "slotViewResetZoom\0slotViewToolbar\0"
    "slotViewBookmarksBar\0slotViewStatusbar\0"
    "slotViewPageSource\0slotViewFullScreen\0"
    "enable\0slotWebSearch\0slotToggleInspector\0"
    "slotAboutApplication\0slotDownloadManager\0"
    "slotSelectLineEdit\0slotAboutToShowBackMenu\0"
    "slotAboutToShowForwardMenu\0"
    "slotAboutToShowWindowMenu\0slotOpenActionUrl\0"
    "QAction*\0action\0slotShowWindow\0"
    "slotSwapFocus\0slotHandlePdfPrinted\0"
    "geometryChangeRequested\0geometry\0"
    "updateToolbarActionText\0visible\0"
    "updateBookmarksToolbarActionText\0"
    "runScriptOnOpenViews"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BrowserMainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      45,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  239,    2, 0x0a /* Public */,
       4,    0,  242,    2, 0x0a /* Public */,
       5,    0,  243,    2, 0x08 /* Private */,
       6,    1,  244,    2, 0x08 /* Private */,
       7,    1,  247,    2, 0x08 /* Private */,
       9,    1,  250,    2, 0x08 /* Private */,
       9,    0,  253,    2, 0x28 /* Private | MethodCloned */,
      11,    1,  254,    2, 0x08 /* Private */,
      12,    0,  257,    2, 0x08 /* Private */,
      13,    0,  258,    2, 0x08 /* Private */,
      14,    0,  259,    2, 0x08 /* Private */,
      15,    0,  260,    2, 0x08 /* Private */,
      16,    0,  261,    2, 0x08 /* Private */,
      17,    0,  262,    2, 0x08 /* Private */,
      18,    0,  263,    2, 0x08 /* Private */,
      19,    0,  264,    2, 0x08 /* Private */,
      20,    0,  265,    2, 0x08 /* Private */,
      21,    0,  266,    2, 0x08 /* Private */,
      22,    0,  267,    2, 0x08 /* Private */,
      23,    0,  268,    2, 0x08 /* Private */,
      24,    0,  269,    2, 0x08 /* Private */,
      25,    0,  270,    2, 0x08 /* Private */,
      26,    0,  271,    2, 0x08 /* Private */,
      27,    0,  272,    2, 0x08 /* Private */,
      28,    0,  273,    2, 0x08 /* Private */,
      29,    0,  274,    2, 0x08 /* Private */,
      30,    0,  275,    2, 0x08 /* Private */,
      31,    0,  276,    2, 0x08 /* Private */,
      32,    1,  277,    2, 0x08 /* Private */,
      34,    0,  280,    2, 0x08 /* Private */,
      35,    1,  281,    2, 0x08 /* Private */,
      36,    0,  284,    2, 0x08 /* Private */,
      37,    0,  285,    2, 0x08 /* Private */,
      38,    0,  286,    2, 0x08 /* Private */,
      39,    0,  287,    2, 0x08 /* Private */,
      40,    0,  288,    2, 0x08 /* Private */,
      41,    0,  289,    2, 0x08 /* Private */,
      42,    1,  290,    2, 0x08 /* Private */,
      45,    0,  293,    2, 0x08 /* Private */,
      46,    0,  294,    2, 0x08 /* Private */,
      47,    1,  295,    2, 0x08 /* Private */,
      48,    1,  298,    2, 0x08 /* Private */,
      50,    1,  301,    2, 0x08 /* Private */,
      52,    1,  304,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      53,    1,  307,    2, 0x02 /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QUrl,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   33,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   33,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 43,   44,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QRect,   49,
    QMetaType::Void, QMetaType::Bool,   51,
    QMetaType::Void, QMetaType::Bool,   51,

 // methods: parameters
    QMetaType::Void, QMetaType::QString,    2,

       0        // eod
};

void BrowserMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BrowserMainWindow *_t = static_cast<BrowserMainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->loadPage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->slotHome(); break;
        case 2: _t->save(); break;
        case 3: _t->slotLoadProgress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->slotUpdateStatusbar((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->slotUpdateWindowTitle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->slotUpdateWindowTitle(); break;
        case 7: _t->loadUrl((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 8: _t->slotPreferences(); break;
        case 9: _t->slotFileNew(); break;
        case 10: _t->slotFileOpen(); break;
        case 11: _t->slotFilePrintPreview(); break;
        case 12: _t->slotFilePrint(); break;
        case 13: _t->slotFilePrintToPDF(); break;
        case 14: _t->slotPrivateBrowsing(); break;
        case 15: _t->slotFileSaveAs(); break;
        case 16: _t->slotEditFind(); break;
        case 17: _t->slotEditFindNext(); break;
        case 18: _t->slotEditFindPrevious(); break;
        case 19: _t->slotShowBookmarksDialog(); break;
        case 20: _t->slotAddBookmark(); break;
        case 21: _t->slotViewZoomIn(); break;
        case 22: _t->slotViewZoomOut(); break;
        case 23: _t->slotViewResetZoom(); break;
        case 24: _t->slotViewToolbar(); break;
        case 25: _t->slotViewBookmarksBar(); break;
        case 26: _t->slotViewStatusbar(); break;
        case 27: _t->slotViewPageSource(); break;
        case 28: _t->slotViewFullScreen((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 29: _t->slotWebSearch(); break;
        case 30: _t->slotToggleInspector((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 31: _t->slotAboutApplication(); break;
        case 32: _t->slotDownloadManager(); break;
        case 33: _t->slotSelectLineEdit(); break;
        case 34: _t->slotAboutToShowBackMenu(); break;
        case 35: _t->slotAboutToShowForwardMenu(); break;
        case 36: _t->slotAboutToShowWindowMenu(); break;
        case 37: _t->slotOpenActionUrl((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 38: _t->slotShowWindow(); break;
        case 39: _t->slotSwapFocus(); break;
        case 40: _t->slotHandlePdfPrinted((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 41: _t->geometryChangeRequested((*reinterpret_cast< const QRect(*)>(_a[1]))); break;
        case 42: _t->updateToolbarActionText((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 43: _t->updateBookmarksToolbarActionText((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 44: _t->runScriptOnOpenViews((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject BrowserMainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_BrowserMainWindow.data,
      qt_meta_data_BrowserMainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *BrowserMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BrowserMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_BrowserMainWindow.stringdata0))
        return static_cast<void*>(const_cast< BrowserMainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int BrowserMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 45)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 45;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 45)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 45;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
